from app_list import *
import os

report = ""
content = '''
module ecs_service_{0} {{
  source               = "./modules/ecs-service"
  ecs_cluster          = "${{module.ecs_cluster.ecs_cluster_id}}"
  alb_listener_443_arn = "${{module.alb_oauth.listener_443}}"
  project_name         = "${{var.project_name}}"
  project_prefix       = "${{var.project_prefix}}"
  oidc_url             = "${{var.oidc_url}}"
  oidc_client_id       = "${{var.oidc_client_id}}"
  oidc_secret          = "${{var.oidc_secret}}"
  vpc_id               = "${{module.vpc.vpc_id}}"
  app_name             = "{0}"
  app_image            = "{1}"
  demo_zone_id         = "${{module.alb_oauth.demo_zone_id}}"
  demo_alb_dns_name    = "${{module.alb_oauth.demo_alb_dns_name}}"
  demo_alb_zone_id     = "${{module.alb_oauth.demo_alb_zone_id}}"
  demo_zone_name       = "${{var.domain["sub"]}}.${{var.domain["name"]}}"
}}

output "{0} DNS name" {{
  value = "{0}.${{var.domain["sub"]}}.${{var.domain["name"]}}"
}}
'''

try:
  for app in apps.keys():
    tf = open(os.getcwd() + "/" + app + "-module.tf", "w")
    tf.write(content.format(app, apps[app]))
    tf.close()
    report = report + "\n" + "Created module file: " + app + "-module.tf"
except Exception as e:
        report = "Problem: ", e

print(report)