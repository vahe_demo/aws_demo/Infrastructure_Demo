Demo:
 - Terraform: 0.11.13
 - Included: GitLab CI/CD EXAMPLE file

---

 - Prerequisite:
   - AWS account with registered Domain Name 
   - OIDC data for OAUTH
   - AWS Key Pair
   - add your application data in app_list.py `apps` (python dict type file)

---

 - Instructions (if not using CI/CD EXAMPLE file): 
   - run `python creator.py`
   - run `terraform init`
   - run `terraform plan -out plan.tfout`
   - run `terraform apply plan.tfout`

---
 - Content:
   - `VPC`: subnets and routes with or without extra cidr block
   - `Bastion`: ASG, Jumphost DNS Hostname, "Visitor" user with limited access  (Commented for now: have issues with AWS Linux 2), Automated hostname takeover on creation, SSM
   - `ECS Cluster`: EC2, ASG, access from Bastion, SSM
   - `ECS Service`: multiple services for multiple apps , ALB Listeners with host-header based routing  and OAUTH
   - `ECS Autoscaling`: Target based policies for ASG and ECS Application. Moved inside ECS modules
   - `ALB`: OAUTH, SSL  Certification, 80 port redirect to 443, DNS Name, default forward to Lambda
   - `Lambda`: redirect to google, no more functions yet.
 
 