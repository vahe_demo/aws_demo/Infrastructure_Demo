module lambda {
  source      = "./modules/lambda"
  lambda_name = "${var.lambda_name}"
  tg_demo_arn = "${module.alb_oauth.tg_demo_arn}"
}
