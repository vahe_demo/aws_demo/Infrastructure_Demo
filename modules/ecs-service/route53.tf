resource aws_route53_record "app" {
  name    = "${var.app_name}."
  zone_id = "${var.demo_zone_id}"
  type    = "A"

  alias {
    name                   = "${var.demo_alb_dns_name}"
    zone_id                = "${var.demo_alb_zone_id}"
    evaluate_target_health = true
  }
}
