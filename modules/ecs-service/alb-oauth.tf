resource aws_lb_listener_rule "demo" {
  listener_arn = "${var.alb_listener_443_arn}"

  action {
    order = 1
    type  = "authenticate-oidc"

    authenticate_oidc {
      authorization_endpoint     = "${var.oidc_url}/oauth/authorize"
      token_endpoint             = "${var.oidc_url}/oauth/token"
      user_info_endpoint         = "${var.oidc_url}/userinfo"
      client_id                  = "${var.oidc_client_id}"
      client_secret              = "${var.oidc_secret}"
      issuer                     = "${var.oidc_url}/oauth/token"
      on_unauthenticated_request = "authenticate"
      scope                      = "openid profile"
    }
  }

  action {
    order            = 2
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.demo.arn}"
  }

  condition {
    field  = "host-header"
    values = ["${var.app_name}.${var.demo_zone_name}"]
  }
}

resource aws_alb_target_group "demo" {
  name     = "${var.project_name}-${var.app_name}"
  port     = "80"
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  deregistration_delay = "0"

  health_check {
    interval            = "30"
    timeout             = "15"
    healthy_threshold   = "3"
    unhealthy_threshold = "7"
    protocol            = "HTTP"
    path                = "/"
    matcher             = "200-399"
  }
}
