variable ecs_cluster {}
variable project_name {}
variable project_prefix {}

variable vpc_id {}

# UAA data
variable oidc_url {}

variable oidc_client_id {}
variable oidc_secret {}

# Application
variable app_name {}

variable app_image {}

variable alb_listener_443_arn {}
variable demo_zone_id {}
variable demo_alb_dns_name {}
variable demo_alb_zone_id {}
variable demo_zone_name {}
