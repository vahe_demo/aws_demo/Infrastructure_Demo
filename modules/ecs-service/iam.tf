# ECS IAM

data aws_iam_policy_document "ecs_service_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com"]
    }
  }
}

resource aws_iam_role "ecs_service" {
  name = "${var.app_name}-ecs-service-role"

  assume_role_policy = "${data.aws_iam_policy_document.ecs_service_assume.json}"
}

resource aws_iam_role_policy "ecs_service" {
  name   = "${var.project_name}-ecs-service-policy"
  role   = "${aws_iam_role.ecs_service.name}"
  policy = "${data.aws_iam_policy_document.ecs_service.json}"
}

data aws_iam_policy_document "ecs_service" {
  statement {
    actions = [
      "ec2:Describe*",
      "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
      "elasticloadbalancing:DeregisterTargets",
      "elasticloadbalancing:Describe*",
      "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
      "elasticloadbalancing:RegisterTargets",
    ]

    resources = [
      "*",
    ]

    effect = "Allow"
  }
}

data aws_iam_policy_document "ecs_task" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource aws_iam_role "ecs_task" {
  name               = "${var.app_name}-task-role"
  assume_role_policy = "${data.aws_iam_policy_document.ecs_task.json}"
}

data aws_iam_policy_document "app_autoscaling_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.application-autoscaling.amazonaws.com"]
    }
  }
}

resource aws_iam_role "app_autoscaling" {
  name = "${var.app_name}-autoscaling-role"

  assume_role_policy = "${data.aws_iam_policy_document.app_autoscaling_assume.json}"
}

resource aws_iam_role_policy "app_autoscaling" {
  name   = "${var.project_prefix}app-autoscaling-pilicy"
  role   = "${aws_iam_role.app_autoscaling.id}"
  policy = "${data.aws_iam_policy_document.app_autoscaling.json}"
}

data aws_iam_policy_document "app_autoscaling" {
  statement {
    actions = [
      "ecs:DescribeServices",
      "ecs:UpdateService",
      "cloudwatch:DeleteAlarms",
      "cloudwatch:DescribeAlarms",
      "cloudwatch:PutMetricAlarm",
    ]

    resources = [
      "*",
    ]

    effect = "Allow"
  }
}
