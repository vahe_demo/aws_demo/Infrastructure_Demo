resource aws_ecs_service "demo" {
  name            = "${var.app_name}"
  cluster         = "${var.ecs_cluster}"
  task_definition = "${aws_ecs_task_definition.app.arn}"
  desired_count   = "1"
  iam_role        = "${aws_iam_role.ecs_service.arn}"

  deployment_maximum_percent         = "100"
  deployment_minimum_healthy_percent = "0"

  load_balancer {
    target_group_arn = "${aws_alb_target_group.demo.arn}"
    container_name   = "${var.app_name}"
    container_port   = "80"
  }

  depends_on = ["aws_lb_listener_rule.demo"]
}

resource aws_ecs_task_definition "app" {
  family       = "${var.project_name}-${var.app_name}"
  network_mode = "bridge"

  container_definitions = "${data.template_file.app_container.rendered}"
  task_role_arn         = "${aws_iam_role.ecs_task.arn}"
}

data template_file "app_container" {
  template = <<JSON
  [
  {
    "name": "$${name}",
    "hostname": "$${name}",
    "essential": true,
    "image": "$${image}",
    "memoryReservation": 128,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 0,
        "protocol": "tcp"
      }
      ]
  }
  ]
  JSON

  vars {
    name  = "${var.app_name}"
    image = "${var.app_image}"
  }
}
