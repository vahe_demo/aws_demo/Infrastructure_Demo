resource aws_iam_instance_profile "bastion" {
  name = "${var.project_prefix}bastion"
  role = "${aws_iam_role.bastion.name}"
}

resource aws_iam_role "bastion" {
  name               = "${var.project_prefix}bastion"
  assume_role_policy = "${data.aws_iam_policy_document.bastion_assume.json}"
}

resource aws_iam_role_policy "bastion" {
  name   = "${var.project_prefix}bastion"
  role   = "${aws_iam_role.bastion.id}"
  policy = "${data.aws_iam_policy_document.bastion.json}"
}

data aws_iam_policy_document "bastion_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data aws_iam_policy_document "bastion" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
    ]

    resources = [
      "arn:aws:logs:*:*:*",
    ]

    effect = "Allow"
  }

  statement {
    sid    = ""
    effect = "Allow"

    actions = [
      "ssmmessages:CreateControlChannel",
      "ssmmessages:CreateDataChannel",
      "ssmmessages:OpenControlChannel",
      "ssmmessages:OpenDataChannel",
      "ssm:UpdateInstanceInformation",
      "route53:ChangeResourceRecordSets",
      "route53:ListResourceRecordSets",
      "route53:ListHostedZones",
      "route53:GetChange",
    ]

    resources = [
      "*",
    ]
  }
}
