data aws_region "current" {}

data aws_ami "aws_linux2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["*amzn2-ami*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data aws_ami "ubuntu" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["*ubuntu*18.04*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource aws_launch_configuration "bastion" {
  image_id                    = "${data.aws_ami.aws_linux2.id}"
  instance_type               = "t2.micro"
  key_name                    = "${var.key_name}"
  iam_instance_profile        = "${aws_iam_instance_profile.bastion.name}"
  security_groups             = ["${aws_security_group.bastion.id}"]
  associate_public_ip_address = true

  user_data         = "${base64encode(data.template_file.user_data.rendered)}"
  enable_monitoring = false

  lifecycle {
    create_before_destroy = true
  }
}

resource aws_autoscaling_group "bastion" {
  name                 = "${var.project_prefix}bastion"
  vpc_zone_identifier  = ["${var.public_subnet_ids}"]
  min_size             = 1
  max_size             = 1
  desired_capacity     = 1
  launch_configuration = "${aws_launch_configuration.bastion.name}"

  tags = [
    {
      key                 = "Name"
      value               = "${var.project_prefix}bastion"
      propagate_at_launch = true
    },
    {
      key                 = "Role"
      value               = "ecs-bastion"
      propagate_at_launch = true
    },
  ]
}

data template_file "bootstrap_vars" {
  template = "${file("${path.module}/files/bootstrap-vars.tpl.yml")}"

  vars {
    vpc_id       = "${var.vpc_id}"
    region_name  = "${data.aws_region.current.name}"
    domain_name  = "${var.domain_name}"
    project_name = "${var.project_name}"
  }
}

data template_file "env_playbook" {
  template = "${file("${path.module}/files/env-playbook.tpl.yml")}"
}

data template_file "user_data" {
  template = "${file("${path.module}/files/user-data.tpl.yml")}"

  vars {
    bootstrap_vars  = "${data.template_file.bootstrap_vars.rendered}"
    env_playbook    = "${data.template_file.env_playbook.rendered}"
    authorized_keys = "${jsonencode(var.authorized_keys)}"
  }
}
