resource aws_security_group "bastion" {
  name        = "public-access"
  description = "allow SSH access"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.project_name}-bastion"
  }
}

resource aws_security_group_rule "access_to_instances" {
  count                    = "${var.security_group_count}"
  type                     = "egress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.bastion.id}"
  source_security_group_id = "${element(var.security_group_ids, count.index)}"
}

resource aws_security_group_rule "access_to_internet" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.bastion.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}
