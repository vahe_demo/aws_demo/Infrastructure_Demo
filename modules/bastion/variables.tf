variable vpc_id {}
variable project_name {}
variable project_prefix {}

variable domain_name {}

variable public_subnet_ids {
  type = "list"
}

variable security_group_ids {
  type    = "list"
  default = [""]
}

variable security_group_count {
  default = 0
}

variable key_name {}

variable authorized_keys {
  type = "list"
}
