resource aws_iam_instance_profile "demo" {
  name = "${var.project_prefix}ecs-instance-profile"
  role = "${aws_iam_role.demo.name}"
}

resource aws_iam_role "demo" {
  name               = "${var.project_prefix}ecs-instance"
  assume_role_policy = "${data.aws_iam_policy_document.ecs_assume.json}"
}

resource aws_iam_role_policy "demo" {
  name   = "${var.project_prefix}instance"
  role   = "${aws_iam_role.demo.id}"
  policy = "${data.aws_iam_policy_document.ecs_instance.json}"
}

data aws_iam_policy_document "ecs_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data aws_iam_policy_document "ecs_instance" {
  statement {
    actions = [
      "ecs:DeregisterContainerInstance",
      "ecs:DiscoverPollEndpoint",
      "ecs:Poll",
      "ecs:RegisterContainerInstance",
      "ecs:StartTelemetrySession",
      "ecs:Submit*",
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
    ]

    resources = [
      "*",
    ]

    effect = "Allow"
  }

  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
    ]

    resources = [
      "arn:aws:logs:*:*:*",
    ]

    effect = "Allow"
  }

  statement {
    sid    = ""
    effect = "Allow"

    actions = [
      "ssmmessages:CreateControlChannel",
      "ssmmessages:CreateDataChannel",
      "ssmmessages:OpenControlChannel",
      "ssmmessages:OpenDataChannel",
      "ssm:UpdateInstanceInformation",
    ]

    resources = [
      "*",
    ]
  }
}
