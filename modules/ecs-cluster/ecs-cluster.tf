resource aws_ecs_cluster "demo" {
  name = "${var.cluster_name}"
}

data aws_ami "ecs_optimized_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["*g-amazon-ecs-optimized*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource aws_launch_configuration "demo" {
  security_groups = ["${aws_security_group.demo_ecs.id}"]

  key_name             = "${var.key_name}"
  image_id             = "${data.aws_ami.ecs_optimized_ami.id}"
  instance_type        = "${var.ecs_instance_type}"
  iam_instance_profile = "${aws_iam_instance_profile.demo.name}"
  user_data            = "${data.template_file.cloud_init.rendered}"

  associate_public_ip_address = false

  lifecycle {
    create_before_destroy = true
  }
}

resource aws_autoscaling_group "demo" {
  name                 = "${var.project_prefix}ecs-cluster"
  vpc_zone_identifier  = ["${var.private_subnet_ids}"]
  min_size             = "${var.asg_min}"
  max_size             = "${var.asg_max}"
  desired_capacity     = "${var.asg_desired}"
  launch_configuration = "${aws_launch_configuration.demo.name}"

  tags = [
    {
      key                 = "Name"
      value               = "${var.project_prefix}ecs-instance"
      propagate_at_launch = true
    },
    {
      key                 = "Role"
      value               = "ecs-instance"
      propagate_at_launch = true
    },
  ]
}

data template_file "cloud_init" {
  template = "${file("${path.module}/files/user-data.tpl.yml")}"

  vars {
    ecs_cluster_name = "${aws_ecs_cluster.demo.name}"
    authorized_keys  = "${var.key_name}"
  }
}
