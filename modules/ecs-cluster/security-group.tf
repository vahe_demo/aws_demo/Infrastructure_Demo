resource aws_security_group "demo_ecs" {
  vpc_id = "${var.vpc_id}"
  name   = "${var.project_name}-ecs-cluster-sg"

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
