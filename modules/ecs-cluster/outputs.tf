output ecs_cluster_id {
  value = "${aws_ecs_cluster.demo.id}"
}

output ecs_asg {
  value = "${aws_autoscaling_group.demo.name}"
}

output ecs_security_group {
  value = "${aws_security_group.demo_ecs.id}"
}
