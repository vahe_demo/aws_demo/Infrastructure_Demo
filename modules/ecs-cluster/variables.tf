variable cluster_name {}
variable project_name {}
variable project_prefix {}

variable vpc_id {}

variable private_subnet_ids {
  type = "list"
}

variable key_name {
  default = ""
}

variable ecs_instance_type {}

variable asg_min {
  default = "1"
}

variable asg_max {
  default = "4"
}

variable asg_desired {
  default = "1"
}

variable cloud_init {
  type    = "map"
  default = {}
}

variable security_groups {
  type    = "list"
  default = []
}

variable associate_public_ip_address {
  default = "false"
}
