resource aws_vpc "demo" {
  cidr_block       = "${var.vpc_cidr_block}"
  instance_tenancy = "${var.vpc_instance_tenancy}"

  tags {
    Name        = "${var.project_name}"
    Environment = "${var.environment}"
    Project     = "${var.project_name}"
  }
}

resource aws_vpc_ipv4_cidr_block_association "extra" {
  count      = "${var.extra_cidr_block != "" ? 1 : 0}"
  vpc_id     = "${aws_vpc.demo.id}"
  cidr_block = "${var.extra_cidr_block}"
}

data aws_availability_zones "demo" {}

resource aws_internet_gateway "demo" {
  vpc_id = "${aws_vpc.demo.id}"

  tags {
    Name = "${var.project_name}"
  }
}

resource aws_route "demo_vpc_internet_access" {
  route_table_id         = "${aws_vpc.demo.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.demo.id}"
}

resource aws_eip "demo_nat" {
  vpc        = true
  depends_on = ["aws_internet_gateway.demo"]
}

resource aws_nat_gateway "demo" {
  subnet_id     = "${aws_subnet.public.0.id}"
  allocation_id = "${aws_eip.demo_nat.id}"
  depends_on    = ["aws_internet_gateway.demo"]
}

resource aws_route_table "private_to_internet" {
  vpc_id = "${aws_vpc.demo.id}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.demo.id}"
  }

  tags {
    Role = "Private"
  }
}

resource aws_route_table_association "private" {
  count          = "${var.az_count}"
  subnet_id      = "${element(data.aws_subnet_ids.private.ids, count.index)}"
  route_table_id = "${aws_route_table.private_to_internet.id}"

  lifecycle {
    ignore_changes = ["subnet_id"]
  }
}

output "vpc_id" {
  value = "${aws_vpc.demo.id}"
}
