resource aws_subnet "public" {
  count             = "${var.az_count}"
  cidr_block        = "${cidrsubnet(aws_vpc.demo.cidr_block, var.extra_cidr_block != "" ? 2 : 3, count.index)}"
  availability_zone = "${element(flatten(data.aws_availability_zones.demo.*.names), count.index)}"
  vpc_id            = "${aws_vpc.demo.id}"

  tags {
    Name = "public-${element(flatten(data.aws_availability_zones.demo.*.names), count.index)}"
    Role = "Public"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource aws_subnet "private_from_extra_cidr" {
  count             = "${var.extra_cidr_block != "" ? var.az_count : 0}"
  cidr_block        = "${cidrsubnet(aws_vpc_ipv4_cidr_block_association.extra.cidr_block, 2, count.index)}"
  availability_zone = "${element(flatten(data.aws_availability_zones.demo.*.names), count.index)}"
  vpc_id            = "${aws_vpc_ipv4_cidr_block_association.extra.vpc_id}"

  tags {
    Name = "private-${element(flatten(data.aws_availability_zones.demo.*.names), count.index)}"
    Role = "Private"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource aws_subnet "private_from_vpc_cidr" {
  count             = "${var.extra_cidr_block == "" ? var.az_count : 0}"
  cidr_block        = "${cidrsubnet(aws_vpc.demo.cidr_block, 3, var.az_count + count.index)}"
  availability_zone = "${element(flatten(data.aws_availability_zones.demo.*.names), count.index)}"
  vpc_id            = "${aws_vpc.demo.id}"

  tags {
    Name = "private-${element(flatten(data.aws_availability_zones.demo.*.names), count.index)}"
    Role = "Private"
  }

  lifecycle {
    create_before_destroy = true
  }
}

data aws_subnet_ids "public" {
  vpc_id = "${aws_vpc.demo.id}"

  filter {
    name   = "tag:Role"
    values = ["Public"]
  }

  depends_on = ["aws_subnet.public"]
}

data aws_subnet_ids "private" {
  vpc_id = "${aws_vpc.demo.id}"

  filter {
    name   = "tag:Role"
    values = ["Private"]
  }

  depends_on = ["aws_subnet.private_from_vpc_cidr", "aws_subnet.private_from_extra_cidr"]
}

output "public_subnet_ids" {
  value = "${data.aws_subnet_ids.public.ids}"
}

output "privte_subnet_ids" {
  value = "${data.aws_subnet_ids.private.ids}"
}
