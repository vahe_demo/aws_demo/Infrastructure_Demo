variable vpc_cidr_block {}
variable vpc_instance_tenancy {}
variable project_name {}
variable environment {}

variable extra_cidr_block {
  default = ""
}

variable az_count {}
