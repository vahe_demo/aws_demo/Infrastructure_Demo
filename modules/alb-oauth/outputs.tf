output listener_443 {
  value = "${aws_alb_listener.listener_443.arn}"
}

output demo_alb_dns_name {
  value = "${aws_alb.demo.dns_name}"
}

output demo_alb_zone_id {
  value = "${aws_alb.demo.zone_id}"
}

output demo_zone_id {
  value = "${aws_route53_zone.demo.zone_id}"
}

output tg_demo_arn {
  value = "${aws_alb_target_group.demo.arn}"
}
