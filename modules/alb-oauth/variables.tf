variable project_name {}
variable domain_name {}
variable subdomain {}

variable vpc_id {}

variable public_subnet_ids {
  type = "list"
}

# OAUTH data
variable oidc_url {}

variable oidc_client_id {}
variable oidc_secret {}
