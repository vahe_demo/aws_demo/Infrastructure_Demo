resource aws_alb "demo" {
  name            = "${var.project_name}"
  subnets         = ["${var.public_subnet_ids}"]
  security_groups = ["${aws_security_group.alb.id}"]
  internal        = "false"
}

resource aws_alb_listener "listener_443" {
  load_balancer_arn = "${aws_alb.demo.id}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-FS-2018-06"
  certificate_arn   = "${aws_acm_certificate.demo.arn}"

  default_action {
    order = 1
    type  = "authenticate-oidc"

    authenticate_oidc {
      authorization_endpoint     = "${var.oidc_url}/oauth/authorize"
      token_endpoint             = "${var.oidc_url}/oauth/token"
      user_info_endpoint         = "${var.oidc_url}/userinfo"
      client_id                  = "${var.oidc_client_id}"
      client_secret              = "${var.oidc_secret}"
      issuer                     = "${var.oidc_url}/oauth/token"
      on_unauthenticated_request = "authenticate"
      scope                      = "openid profile"
    }
  }

  default_action {
    order            = 2
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.demo.arn}"
  }
}

resource aws_alb_listener "listener_80" {
  load_balancer_arn = "${aws_alb.demo.id}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      protocol    = "HTTPS"
      port        = "443"
      status_code = "HTTP_302"
    }
  }
}

resource aws_alb_target_group "demo" {
  name        = "demo-lambda"
  target_type = "lambda"
}
