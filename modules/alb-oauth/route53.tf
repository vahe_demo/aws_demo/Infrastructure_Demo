data aws_route53_zone "demo" {
  name = "${var.domain_name}"
}

resource aws_route53_zone "demo" {
  name = "${var.subdomain}.${var.domain_name}"
}

resource aws_route53_record "demo" {
  name    = ""
  zone_id = "${aws_route53_zone.demo.zone_id}"
  type    = "A"

  alias {
    name                   = "${aws_alb.demo.dns_name}"
    zone_id                = "${aws_alb.demo.zone_id}"
    evaluate_target_health = true
  }
}

resource aws_route53_record "add_to_sdl" {
  name    = "${var.subdomain}.${var.domain_name}"
  zone_id = "${data.aws_route53_zone.demo.zone_id}"
  type    = "NS"
  ttl     = 300

  records = [
    "${aws_route53_zone.demo.name_servers.0}",
    "${aws_route53_zone.demo.name_servers.1}",
    "${aws_route53_zone.demo.name_servers.2}",
    "${aws_route53_zone.demo.name_servers.3}",
  ]
}

resource aws_acm_certificate "demo" {
  domain_name               = "${var.subdomain}.${var.domain_name}"
  subject_alternative_names = ["*.${var.subdomain}.${var.domain_name}"]
  validation_method         = "DNS"

  tags = {
    Name = "demo_cert"
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on = ["aws_route53_record.add_to_sdl"]
}

resource aws_route53_record "cert_validation" {
  name       = "${aws_acm_certificate.demo.domain_validation_options.0.resource_record_name}"
  type       = "${aws_acm_certificate.demo.domain_validation_options.0.resource_record_type}"
  zone_id    = "${aws_route53_zone.demo.zone_id}"
  records    = ["${aws_acm_certificate.demo.domain_validation_options.0.resource_record_value}"]
  ttl        = 60
  depends_on = ["aws_acm_certificate.demo"]
}

resource aws_acm_certificate_validation "demo" {
  certificate_arn         = "${aws_acm_certificate.demo.arn}"
  validation_record_fqdns = ["${aws_route53_record.cert_validation.fqdn}"]
  depends_on              = ["aws_route53_record.cert_validation"]
}
