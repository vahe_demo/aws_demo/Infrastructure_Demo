data aws_iam_policy_document "demo_lambda_assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource aws_iam_role "demo_lambda" {
  name               = "alb-lambda"
  assume_role_policy = "${data.aws_iam_policy_document.demo_lambda_assume_role.json}"
}

resource aws_iam_role_policy_attachment "demo_lambda" {
  role       = "${aws_iam_role.demo_lambda.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
