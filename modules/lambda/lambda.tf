data archive_file "lambda_code" {
  type        = "zip"
  output_path = "${path.module}/files/lambda_code.zip"

  source {
    filename = "lambda_function.py"

    content = <<LAMBDA
def lambda_handler(event, context):
    return {
        'statusCode': 301,
        'headers': {
            'Location': 'https://google.com'
        }
    }
LAMBDA
  }
}

resource aws_lambda_function "demo" {
  filename         = "${path.module}/files/lambda_code.zip"
  function_name    = "${var.lambda_name}"
  role             = "${aws_iam_role.demo_lambda.arn}"
  handler          = "lambda_function.lambda_handler"
  runtime          = "python3.6"
  source_code_hash = "${data.archive_file.lambda_code.output_base64sha256}"
  depends_on       = ["data.archive_file.lambda_code"]
}

resource aws_lambda_permission "demo" {
  statement_id  = "AllowExecutionFromlb"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.demo.arn}"
  principal     = "elasticloadbalancing.amazonaws.com"
  source_arn    = "${var.tg_demo_arn}"
}

resource aws_lb_target_group_attachment "demo_lambda" {
  target_group_arn = "${var.tg_demo_arn}"
  target_id        = "${aws_lambda_function.demo.arn}"
  depends_on       = ["aws_lambda_permission.demo"]
}
