module vpc {
  source               = "./modules/vpc"
  project_name         = "${var.project_name}"
  environment          = "${var.environment}"
  vpc_cidr_block       = "${var.vpc_cidr_block}"
  extra_cidr_block     = "${var.extra_cidr_block}"
  vpc_instance_tenancy = "${var.vpc_instance_tenancy}"
  az_count             = "${var.az_count}"
}
