module alb_oauth {
  source            = "./modules/alb-oauth"
  project_name      = "${var.project_name}"
  domain_name       = "${var.domain["name"]}"
  subdomain         = "${var.domain["sub"]}"
  oidc_url          = "${var.oidc_url}"
  oidc_client_id    = "${var.oidc_client_id}"
  oidc_secret       = "${var.oidc_secret}"
  public_subnet_ids = "${module.vpc.public_subnet_ids}"
  vpc_id            = "${module.vpc.vpc_id}"
}
