module bastion {
  source               = "./modules/bastion"
  project_name         = "${var.project_name}"
  project_prefix       = "${var.project_prefix}"
  vpc_id               = "${module.vpc.vpc_id}"
  key_name             = "${var.main_key}"
  security_group_ids   = ["${module.ecs_cluster.ecs_security_group}"]
  security_group_count = 1
  domain_name          = "${var.domain["name"]}"

  public_subnet_ids = [
    "${module.vpc.public_subnet_ids}",
  ]

  authorized_keys = "${var.authorized_keys}"
}
