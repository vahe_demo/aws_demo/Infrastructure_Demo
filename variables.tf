# AWS
variable aws_region {
  default = "eu-central-1"
}

variable az_count {
  default = 2
}

# Project
variable environment {
  default = "demo"
}

variable project_name {
  default = "vahe-demo"
}
variable project_prefix {
  default = "v-demo"
}

# VPC
variable vpc_instance_tenancy {
  default = "default"
}

variable vpc_cidr_block {
  default = "10.0.0.0/16"
}

variable extra_cidr_block {
  default = ""
}

# Route53
variable domain {
  type = "map"

  default = {
    name = "main_domain_name.com"
    sub  = "subdomain_name"
  }
}

# ECS Cluster
variable ecs_cluster_name {
  default = "demo"
}

variable ecs_instance_type {
  default = "t2.xlarge"
}

variable asg_min {
  default = 1
}

variable asg_max {
  default = 2
}

variable asg_desired {
  default = 1
}

# OAUTH data
variable oidc_url {}

variable oidc_client_id {}
variable oidc_secret {}

# Application List: 
# DEPRECATED, use app_list.py instead
variable apps {
  type = "map"

  default = {
    app01 = "nginx:latest"
    app02 = "httpd:2.4"
  }
}

# SSH
variable main_key {
  default = "KEY_PAIR_NAME"
}

variable authorized_keys {
  type    = "list"
  default = [""]
}

# Lambda
variable lambda_name {
  default = "demo_lambda"
}
