module ecs_cluster {
  source            = "./modules/ecs-cluster"
  project_name      = "${var.project_name}"
  project_prefix    = "${var.project_prefix}"
  cluster_name      = "${var.ecs_cluster_name}"
  vpc_id            = "${module.vpc.vpc_id}"
  ecs_instance_type = "${var.ecs_instance_type}"

  private_subnet_ids = [
    "${module.vpc.privte_subnet_ids}",
  ]

  key_name = "${var.main_key}"
}
